package com.library.bookmanagementservice.repository;

import com.library.bookmanagementservice.domain.Author;
import com.library.bookmanagementservice.domain.Book;
import com.library.bookmanagementservice.domain.BookCategory;
import com.library.bookmanagementservice.domain.Publisher;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface BookRepository extends JpaRepository<Book, Long> {


    List<Book> findByAuthor(Author author);

    List<Book> findByCategory(BookCategory category);

    List<Book> findByPublisher(Publisher publisher);
}
