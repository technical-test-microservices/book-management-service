package com.library.bookmanagementservice.repository;


import com.library.bookmanagementservice.domain.BookCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookCategoryRepository extends JpaRepository<BookCategory, Long> {

    boolean existsByCategoryName(String categoryName);

}