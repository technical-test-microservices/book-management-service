package com.library.bookmanagementservice.service;

import com.library.bookmanagementservice.domain.Book;
import com.library.bookmanagementservice.domain.Publisher;
import com.library.bookmanagementservice.exception.CustomBadRequestException;
import com.library.bookmanagementservice.exception.CustomNotFoundException;
import com.library.bookmanagementservice.exception.GeneralBodyResponse;
import com.library.bookmanagementservice.repository.BookRepository;
import com.library.bookmanagementservice.repository.PublisherRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashSet;
import java.util.List;

@Service
public class PublisherService {

    private final PublisherRepository publisherRepository;
    
    private final BookRepository bookRepository;

    public PublisherService(PublisherRepository publisherRepository, BookRepository bookRepository) {
        this.publisherRepository = publisherRepository;
        this.bookRepository = bookRepository;
    }

    @Transactional
    public GeneralBodyResponse createPublisher(Publisher publisher) {
        if (publisher.getBooks() == null) {
            publisher.setBooks(new HashSet<>());
        }

        publisher.setCreatedAt(new Date());
        publisher.setPublisherName(publisher.getPublisherName());
        if (publisher == null || publisher.getPublisherName() == null) {
            throw new CustomBadRequestException(400, HttpStatus.BAD_REQUEST.getReasonPhrase(), "Publisher name cannot be null.");
        }

        Publisher createdPublisher = publisherRepository.save(publisher);
        return new GeneralBodyResponse(201, HttpStatus.CREATED.getReasonPhrase(), "Successfully created Publisher", createdPublisher);
    }



    @Transactional
    public GeneralBodyResponse updatePublisher(Long publisherId, Publisher updatedPublisher) {
        Publisher existingPublisher = publisherRepository.findById(publisherId)
                .orElseThrow(() -> new CustomNotFoundException(404, HttpStatus.NOT_FOUND.getReasonPhrase(), "Publisher not found with ID: " + publisherId));

        if (updatedPublisher.getPublisherName() != null) {
            existingPublisher.setPublisherName(updatedPublisher.getPublisherName());
        }
        existingPublisher.setUpdatedAt(new Date());
        existingPublisher.setAddress(updatedPublisher.getAddress());
        existingPublisher.setPhoneNumber(updatedPublisher.getPhoneNumber());
        existingPublisher.setEmail(updatedPublisher.getEmail());

        Publisher updatedPublisherEntity = publisherRepository.save(existingPublisher);
        return new GeneralBodyResponse(200, HttpStatus.OK.getReasonPhrase(), "Successfully updated Publisher", updatedPublisherEntity);
    }


    @Transactional
    public GeneralBodyResponse deletePublisher(Long publisherId) {
        Publisher publisher = publisherRepository.findById(publisherId)
                .orElseThrow(() -> new CustomNotFoundException(404,HttpStatus.NOT_FOUND.getReasonPhrase(), "Publisher not found with ID: " + publisherId));

        publisherRepository.delete(publisher);
        return new GeneralBodyResponse(200,HttpStatus.OK.getReasonPhrase(), "Successfully deleted Publisher", publisherId);
    }

    public GeneralBodyResponse getPublisherById(Long publisherId) {
        Publisher publisher = publisherRepository.findById(publisherId)
                .orElseThrow(() -> new CustomNotFoundException(404,HttpStatus.NOT_FOUND.getReasonPhrase(),"Publisher not found with ID: " + publisherId));
        return new GeneralBodyResponse(200, HttpStatus.OK.getReasonPhrase(), "Successfully retrieved Publisher", publisher);
    }

    public GeneralBodyResponse getAllPublishers() {
        List<Publisher> publishers = publisherRepository.findAll();
        if (publishers.isEmpty()) {
            throw new CustomNotFoundException(404,HttpStatus.NOT_FOUND.getReasonPhrase(),"No publishers found.");
        }
        return new GeneralBodyResponse(200,HttpStatus.OK.getReasonPhrase(), "Successfully retrieved all Publishers", publishers);
    }

    @Transactional(readOnly = true)
    public GeneralBodyResponse getBooksByPublisher(Long publisherId) {
        Publisher publisher = publisherRepository.findById(publisherId)
                .orElseThrow(() -> new CustomNotFoundException(404, HttpStatus.NOT_FOUND.getReasonPhrase(), "Publisher not found with ID: " + publisherId));

        List<Book> books = bookRepository.findByPublisher(publisher);
        if (books.isEmpty()) {
            throw new CustomNotFoundException(404, HttpStatus.NOT_FOUND.getReasonPhrase(), "No books found for publisher with ID: " + publisherId);
        }

        return new GeneralBodyResponse(200, HttpStatus.OK.getReasonPhrase(), "Books published by publisher retrieved successfully", books);
    }

}
