package com.library.bookmanagementservice.service;

import com.library.bookmanagementservice.domain.Author;
import com.library.bookmanagementservice.domain.Book;
import com.library.bookmanagementservice.exception.CustomBadRequestException;
import com.library.bookmanagementservice.exception.CustomNotFoundException;
import com.library.bookmanagementservice.exception.GeneralBodyResponse;
import com.library.bookmanagementservice.repository.AuthorRepository;
import com.library.bookmanagementservice.repository.BookRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class AuthorService {

    private final AuthorRepository authorRepository;
    
    private final BookRepository bookRepository;
    
    

    public AuthorService(AuthorRepository authorRepository, BookRepository bookRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
        
    }

    @Transactional
    public GeneralBodyResponse createAuthor(Author author) {
        author.setCreatedAt(new Date());
        author.setPhoneNumber(author.getPhoneNumber());
        if (author == null || author.getAuthorName() == null) {
            throw new CustomBadRequestException(400, HttpStatus.BAD_REQUEST.getReasonPhrase(), "Author name cannot be null.");
        }

        authorRepository.save(author);
        return new GeneralBodyResponse(201, HttpStatus.CREATED.getReasonPhrase(), "Successfully created Author", author);
    }

    @Transactional
    public GeneralBodyResponse updateAuthor(Long authorId, Author updatedAuthor) {
        Author existingAuthor = authorRepository.findById(authorId)
                .orElseThrow(() -> new CustomNotFoundException(404, HttpStatus.NOT_FOUND.getReasonPhrase(), "Author not found with ID: " + authorId));

        if (updatedAuthor.getAuthorName() != null) {
            existingAuthor.setAuthorName(updatedAuthor.getAuthorName());
        }
        existingAuthor.setUpdatedAt(new Date());
        existingAuthor.setAddress(updatedAuthor.getAddress());
        existingAuthor.setPhoneNumber(updatedAuthor.getPhoneNumber());
        existingAuthor.setEmail(updatedAuthor.getEmail());

        authorRepository.save(existingAuthor);
        return new GeneralBodyResponse(200, HttpStatus.OK.getReasonPhrase(), "Successfully updated Author", existingAuthor);
    }


    @Transactional
    public GeneralBodyResponse deleteAuthor(Long authorId) {
        Author author = authorRepository.findById(authorId)
                .orElseThrow(() -> new CustomNotFoundException(404, HttpStatus.NOT_FOUND.getReasonPhrase(), "Author not found with ID : " + authorId));

        authorRepository.delete(author);
        return new GeneralBodyResponse(200, HttpStatus.OK.getReasonPhrase(), "Successfully delete Author", authorId);
    }

    public GeneralBodyResponse getAuthorById(Long authorId) {
        Author author = authorRepository.findById(authorId)
                .orElseThrow(() -> new CustomNotFoundException(404, HttpStatus.NOT_FOUND.getReasonPhrase(), "Author not found with ID: " + authorId));
        return new GeneralBodyResponse(200, HttpStatus.OK.getReasonPhrase(), "Author details retrieved successfully", author);
    }

    public GeneralBodyResponse getAllAuthors() {
        List<Author> authors = authorRepository.findAll();
        if (authors.isEmpty()) {
            throw new CustomNotFoundException(404, HttpStatus.NOT_FOUND.getReasonPhrase(), "No authors found.");
        }
        return new GeneralBodyResponse(200, HttpStatus.OK.getReasonPhrase(), "Authors list retrieved successfully", authors);
    }

    @Transactional(readOnly = true)
    public GeneralBodyResponse getBooksByAuthor(Long authorId) {
        Author author = authorRepository.findById(authorId)
                .orElseThrow(() -> new CustomNotFoundException(404, HttpStatus.NOT_FOUND.getReasonPhrase(), "Author not found with ID: " + authorId));

        List<Book> books = bookRepository.findByAuthor(author);
        if (books.isEmpty()) {
            throw new CustomNotFoundException(404, HttpStatus.NOT_FOUND.getReasonPhrase(), "No books found for author with ID: " + authorId);
        }

        return new GeneralBodyResponse(200, HttpStatus.OK.getReasonPhrase(), "Books written by author retrieved successfully", books);
    }

}
