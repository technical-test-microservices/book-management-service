package com.library.bookmanagementservice.service;

import com.library.bookmanagementservice.domain.Book;
import com.library.bookmanagementservice.exception.CustomBadRequestException;
import com.library.bookmanagementservice.exception.CustomNotFoundException;
import com.library.bookmanagementservice.exception.GeneralBodyResponse;
import com.library.bookmanagementservice.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class BookService {

    private final BookRepository bookRepository;

    @Autowired
    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Transactional
    public GeneralBodyResponse createBook(Book book) {
        book.setCreatedAt(new Date());
        book.setAvailable(true);
        if (book == null || book.getTitle() == null) {
            throw new CustomBadRequestException(400, HttpStatus.BAD_REQUEST.getReasonPhrase(), "Book title cannot be null.");
        }

        bookRepository.save(book);
        return new GeneralBodyResponse(201, HttpStatus.CREATED.getReasonPhrase(), "Successfully created Book", book);
    }

    @Transactional
    public GeneralBodyResponse updateBook(Long bookId, Book updatedBook) {
        Book existingBook = bookRepository.findById(bookId)
                .orElseThrow(() -> new CustomNotFoundException(404, HttpStatus.NOT_FOUND.getReasonPhrase(), "Book not found with ID: " + bookId));

        if (updatedBook.getTitle() != null) {
            existingBook.setTitle(updatedBook.getTitle());
        }
        existingBook.setUpdatedAt(new Date());
        existingBook.setAuthor(updatedBook.getAuthor());
        bookRepository.save(existingBook);
        return new GeneralBodyResponse(200, HttpStatus.OK.getReasonPhrase(), "Successfully updated Book", existingBook);
    }

    @Transactional
    public GeneralBodyResponse deleteBook(Long bookId) {
        Book book = bookRepository.findById(bookId)
                .orElseThrow(() -> new CustomNotFoundException(404, HttpStatus.NOT_FOUND.getReasonPhrase(), "Book not found with ID : " + bookId));

        bookRepository.delete(book);
        return new GeneralBodyResponse(200, HttpStatus.OK.getReasonPhrase(), "Successfully delete Book", bookId);
    }

    public GeneralBodyResponse getBookById(Long bookId) {
        Book book = bookRepository.findById(bookId)
                .orElseThrow(() -> new CustomNotFoundException(404, HttpStatus.NOT_FOUND.getReasonPhrase(), "Book not found with ID: " + bookId));
        return new GeneralBodyResponse(200, HttpStatus.OK.getReasonPhrase(), "Book details retrieved successfully", book);
    }

    public GeneralBodyResponse getAllBooks() {
        List<Book> books = bookRepository.findAll();
        if (books.isEmpty()) {
            throw new CustomNotFoundException(404, HttpStatus.NOT_FOUND.getReasonPhrase(), "No books found.");
        }
        return new GeneralBodyResponse(200, HttpStatus.OK.getReasonPhrase(), "Books list retrieved successfully", books);
    }

}
