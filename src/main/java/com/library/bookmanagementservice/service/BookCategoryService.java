package com.library.bookmanagementservice.service;

import com.library.bookmanagementservice.domain.Book;
import com.library.bookmanagementservice.domain.BookCategory;
import com.library.bookmanagementservice.exception.CustomBadRequestException;
import com.library.bookmanagementservice.exception.CustomNotFoundException;
import com.library.bookmanagementservice.exception.GeneralBodyResponse;
import com.library.bookmanagementservice.repository.BookCategoryRepository;
import com.library.bookmanagementservice.repository.BookRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Set;

@Service
public class BookCategoryService {


    private final BookCategoryRepository categoryRepository;
    
    private final BookRepository bookRepository;

    public BookCategoryService(BookCategoryRepository categoryRepository, BookRepository bookRepository) {
        this.categoryRepository = categoryRepository;
        this.bookRepository = bookRepository;
    }

    @Transactional
    public GeneralBodyResponse createCategory(BookCategory category) {
        if (category == null || category.getCategoryName() == null) {
            throw new CustomBadRequestException(400, HttpStatus.BAD_REQUEST.getReasonPhrase(), "Category name cannot be null.");
        }
        if (categoryRepository.existsByCategoryName(category.getCategoryName())) {
            throw new CustomBadRequestException(400, HttpStatus.BAD_REQUEST.getReasonPhrase(), "Category with name '" + category.getCategoryName() + "' already exists.");
        }

        category.setCreatedAt(new Date());
        category.setUpdatedAt(new Date());

        Set<Book> books = category.getBooks();
        if (books != null && !books.isEmpty()) {
            for (Book book : books) {
                book.setCategory(category);
            }
        }

        BookCategory savedCategory = categoryRepository.save(category);
        return new GeneralBodyResponse(201, HttpStatus.CREATED.getReasonPhrase(), "Successfully created category", savedCategory);
    }


    @Transactional
    public GeneralBodyResponse updateCategory(Long categoryId, BookCategory updatedCategory) {
        if (updatedCategory == null || updatedCategory.getCategoryName() == null) {
            throw new CustomBadRequestException(400, HttpStatus.BAD_REQUEST.getReasonPhrase(), "Updated category name cannot be null.");
        }

        BookCategory existingCategory = categoryRepository.findById(categoryId)
                .orElseThrow(() -> new CustomNotFoundException(404, HttpStatus.NOT_FOUND.getReasonPhrase(), "Category not found with ID: " + categoryId));

        if (!existingCategory.getCategoryName().equals(updatedCategory.getCategoryName()) &&
                categoryRepository.existsByCategoryName(updatedCategory.getCategoryName())) {
            throw new CustomBadRequestException(400, HttpStatus.BAD_REQUEST.getReasonPhrase(), "Category with name '" + updatedCategory.getCategoryName() + "' already exists.");
        }

        existingCategory.setCategoryName(updatedCategory.getCategoryName());
        existingCategory.setDescription(updatedCategory.getDescription());
        existingCategory.setUpdatedAt(new Date());

        categoryRepository.save(existingCategory);
        return new GeneralBodyResponse(200, HttpStatus.OK.getReasonPhrase(), "Successfully updated category", existingCategory);
    }


    @Transactional
    public GeneralBodyResponse deleteCategory(Long categoryId) {
        BookCategory category = categoryRepository.findById(categoryId)
                .orElseThrow(() -> new CustomNotFoundException(404, HttpStatus.NOT_FOUND.getReasonPhrase(), "Category not found with ID: " + categoryId));

        categoryRepository.delete(category);
        return new GeneralBodyResponse(200, HttpStatus.OK.getReasonPhrase(), "Successfully deleted category with ID : " + categoryId, categoryId);
    }

    public GeneralBodyResponse getCategoryById(Long categoryId) {
        BookCategory category = categoryRepository.findById(categoryId)
                .orElseThrow(() -> new CustomNotFoundException(404, HttpStatus.NOT_FOUND.getReasonPhrase(), "Category not found with ID: " + categoryId));
        return new GeneralBodyResponse(200, HttpStatus.OK.getReasonPhrase(), "Category details retrieved successfully", category);
    }

    public GeneralBodyResponse getAllCategories() {
        List<BookCategory> categories = categoryRepository.findAll();
        if (categories.isEmpty()) {
            throw new CustomNotFoundException(404, HttpStatus.NOT_FOUND.getReasonPhrase(), "No categories found.");
        }
        return new GeneralBodyResponse(200, HttpStatus.OK.getReasonPhrase(), "Categories list retrieved successfully", categories);
    }

    @Transactional(readOnly = true)
    public GeneralBodyResponse getBooksByCategory(Long categoryId) {
        BookCategory bookCategory = categoryRepository.findById(categoryId)
                .orElseThrow(() -> new CustomNotFoundException(404, HttpStatus.NOT_FOUND.getReasonPhrase(), "Category not found with ID: " + categoryId));

        List<Book> books = bookRepository.findByCategory(bookCategory);
        if (books.isEmpty()) {
            throw new CustomNotFoundException(404, HttpStatus.NOT_FOUND.getReasonPhrase(), "No books found in category with ID: " + categoryId);
        }

        return new GeneralBodyResponse(200, HttpStatus.OK.getReasonPhrase(), "Books in category retrieved successfully", books);
    }

}
