package com.library.bookmanagementservice.controller;

import com.library.bookmanagementservice.domain.Book;
import com.library.bookmanagementservice.exception.CustomNotFoundException;
import com.library.bookmanagementservice.exception.GeneralBodyResponse;
import com.library.bookmanagementservice.service.BookService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/books")
public class BookController {

    private final BookService bookService;

    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @PostMapping("/create")
    public ResponseEntity<GeneralBodyResponse> createBook(@Valid @RequestBody Book book) {
        GeneralBodyResponse response = bookService.createBook(book);
        return ResponseEntity.status(response.getCode()).body(response);
    }

    @GetMapping("/{id}")
    public ResponseEntity<GeneralBodyResponse> getBookById(@PathVariable Long id) {
        GeneralBodyResponse response = bookService.getBookById(id);
        return ResponseEntity.status(response.getCode()).body(response);
    }

    @PutMapping("/{id}/updateAvailability")
    public ResponseEntity<GeneralBodyResponse> updateBookAvailability(@PathVariable Long id, @RequestParam boolean available) {
        Book updatedBook = new Book();
        updatedBook.setAvailable(available);

        try {
            GeneralBodyResponse response = bookService.updateBook(id, updatedBook);
            return ResponseEntity.status(response.getCode()).body(response);
        } catch (CustomNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new GeneralBodyResponse(e.getCode(), HttpStatus.NOT_FOUND.getReasonPhrase(), e.getMessage(), null));        }
    }

    @DeleteMapping("/{id}/delete")
    public ResponseEntity<String> deleteBookById(@PathVariable Long id) {
        GeneralBodyResponse response = bookService.deleteBook(id);
        return ResponseEntity.status(response.getCode()).body(response.getMessage());
    }
}