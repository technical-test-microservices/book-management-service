package com.library.bookmanagementservice.controller;

import com.library.bookmanagementservice.domain.BookCategory;
import com.library.bookmanagementservice.exception.GeneralBodyResponse;
import com.library.bookmanagementservice.service.BookCategoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/categories")
public class BookCategoryController {
    
    private final BookCategoryService categoryService;

    public BookCategoryController(BookCategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @PostMapping
    public ResponseEntity<?> createCategory(@RequestBody BookCategory category) {
        GeneralBodyResponse response = categoryService.createCategory(category);
        return ResponseEntity.ok().body(response);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateCategory(@PathVariable Long id, @RequestBody BookCategory category) {
            GeneralBodyResponse response = categoryService.updateCategory(id, category);
            return ResponseEntity.ok().body(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCategory(@PathVariable Long id) {
            GeneralBodyResponse response = categoryService.deleteCategory(id);
            return ResponseEntity.ok().body(response);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getCategoryById(@PathVariable Long id) {
            GeneralBodyResponse response = categoryService.getCategoryById(id);
            return ResponseEntity.ok().body(response);
    }

    @GetMapping
    public ResponseEntity<?> getAllCategories() {
        GeneralBodyResponse response = categoryService.getAllCategories();
        return ResponseEntity.ok().body(response);
    }

    @GetMapping("/{categoryId}/books")
    public ResponseEntity<GeneralBodyResponse> getBooksByCategory(@PathVariable Long categoryId) {
        GeneralBodyResponse response = categoryService.getBooksByCategory(categoryId);
        return ResponseEntity.ok().body(response);
    }
}
