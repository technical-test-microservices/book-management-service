package com.library.bookmanagementservice.controller;

import com.library.bookmanagementservice.domain.Publisher;
import com.library.bookmanagementservice.exception.GeneralBodyResponse;
import com.library.bookmanagementservice.service.PublisherService;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/publishers")
public class PublisherController {
    
    private final PublisherService publisherService;

    public PublisherController(PublisherService publisherService) {
        this.publisherService = publisherService;
    }

    @PostMapping
    public ResponseEntity<?> createPublisher(@Valid @RequestBody Publisher publisher) {
        GeneralBodyResponse response = publisherService.createPublisher(publisher);
        return ResponseEntity
                .ok()
                .body(response);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updatePublisher(@PathVariable Long id, @Valid @RequestBody Publisher publisher) {
        GeneralBodyResponse response = publisherService.updatePublisher(id, publisher);
        return ResponseEntity
                .ok()
                .body(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletePublisher(@PathVariable Long id) {
        GeneralBodyResponse response = publisherService.deletePublisher(id);
        return ResponseEntity
                .ok()
                .body(response);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getPublisherById(@PathVariable Long id) {
        GeneralBodyResponse response = publisherService.getPublisherById(id);
        return ResponseEntity
                .ok()
                .body(response);
    }

    @GetMapping
    public ResponseEntity<?> getAllPublishers() {
        GeneralBodyResponse response = publisherService.getAllPublishers();
        return ResponseEntity
                .ok()
                .body(response);
    }

    @GetMapping("/{publisherId}/books")
    public ResponseEntity<GeneralBodyResponse> getBooksByPublisher(@PathVariable Long publisherId) {
        GeneralBodyResponse response = publisherService.getBooksByPublisher(publisherId);
        return ResponseEntity
                .ok()
                .body(response);    
    }
}
