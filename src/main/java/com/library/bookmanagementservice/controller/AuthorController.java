package com.library.bookmanagementservice.controller;

import com.library.bookmanagementservice.domain.Author;
import com.library.bookmanagementservice.exception.GeneralBodyResponse;
import com.library.bookmanagementservice.service.AuthorService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/authors")
@Transactional
public class AuthorController {
    
    private final AuthorService authorService;

    @PersistenceContext
    private EntityManager entityManager;

    public AuthorController(AuthorService authorService, EntityManager entityManager) {
        this.authorService = authorService;
        this.entityManager = entityManager;
    }

    @PostMapping
    public ResponseEntity<?> createAuthor(@Valid @RequestBody Author author) {
        GeneralBodyResponse response = authorService.createAuthor(author);
        return ResponseEntity.ok().body(response);
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> updateAuthor(@PathVariable Long id, @Valid @RequestBody Author author) {
        GeneralBodyResponse response = authorService.updateAuthor(id, author);
        return ResponseEntity
                .ok()
                .body(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteAuthor(@PathVariable Long id) {
        GeneralBodyResponse response = authorService.deleteAuthor(id);
        return ResponseEntity
                .ok()
                .body(response);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getAuthorById(@PathVariable Long id) {
        GeneralBodyResponse response = authorService.getAuthorById(id);
        return ResponseEntity
                .ok()
                .body(response);
    }

    @GetMapping
    public ResponseEntity<?> getAllAuthors() {
        GeneralBodyResponse response = authorService.getAllAuthors();
        return ResponseEntity
                .ok()
                .body(response);
    }

    @GetMapping("/{authorId}/books")
    public ResponseEntity<GeneralBodyResponse> getBooksByAuthor(@PathVariable Long authorId) {
        GeneralBodyResponse response = authorService.getBooksByAuthor(authorId);
        return ResponseEntity
                .ok()
                .body(response);
    }
}
